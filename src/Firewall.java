import java.io.FileNotFoundException;
import java.util.*;
import java.io.File;
import java.util.regex.Pattern;

/**
 * Created by owner on 20/10/17.
 */

public class Firewall {
    private ArrayList[] table; //2d arraylist to store rules(where each row is a set of rules)

    private Firewall(String csv) {
        this.table = new ArrayList[6];
        table[0] = new ArrayList<String>(); //direction column
        table[1] = new ArrayList<String>(); //protocol column
        table[2] = new ArrayList<Integer>(); //portMin column
        table[3] = new ArrayList<Integer>(); //portMax column
        table[4] = new ArrayList<Integer>(); //ipMin column
        table[5] = new ArrayList<Integer>(); //ipMax column
        File f = new File(csv);
        //https://www.youtube.com/watch?v=3_40oiUdLG8&t=278s this tutorial helped a bit with the scanning portion below
        try {
            Scanner input = new Scanner(f);
            while (input.hasNext()) {
                String line = input.next();
                String[] values = line.split(","); //splits the current csv file line into
                table[0].add(values[0]); //adds first item before comma(direction) to the direction column
                table[1].add(values[1]); //likewise for protocol
                if (values[2].contains("-")) { //if the port is a range
                    String[] portRange = values[2].split("-");
                    table[2].add(Integer.parseInt(portRange[0])); //lower port bound
                    table[3].add(Integer.parseInt(portRange[1])); //higher port bound
                }
                else { //if port is not a range, then lower and upper bound are equal
                    table[2].add(Integer.parseInt(values[2]));
                    table[3].add(Integer.parseInt(values[2]));
                }
                if (values[3].contains("-")) { //if ip address is a range
                    String[] ipRange = values[3].split("-");
                    int ipMin = parseIp(ipRange[0]); //lower ip bound
                    int ipMax = parseIp(ipRange[1]); //higher ip bound
                    table[4].add(ipMin);
                    table[5].add(ipMax);
                }
                else {
                    int ip = parseIp(values[3]); //if ip is not a range, then lower and upper bound are equal
                    table[4].add(ip);
                    table[5].add(ip);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    private boolean accept_packet(String direction, String protocol, int port, String ip_address) {
        for (int i = 0; i < table[0].size(); i++) { //checks every row, aka every set of rules
            if (direction.equals(table[0].get(i)) && protocol.equals(table[1].get(i))) { //if the protocol and direction match
                int minPort = (int) table[2].get(i); //here I am getting the bounds for port and ip for the comparison
                int maxPort = (int) table[3].get(i);
                int ipMin = (int) table[4].get(i);
                int ipMax = (int) table[5].get(i);
                int ip = parseIp(ip_address); //converts string ip address
                if (port >= minPort && port <= maxPort
                        && ip >= ipMin && ip <= ipMax) {
                    return true; //everything matches!
                }
            }
        }
        return false; //if none of the sets of rules matches the input arguments
    }

    //https://stackoverflow.com/questions/12057853/how-to-convert-string-ip-numbers-to-integer-in-java
    private static int parseIp(String address) {
        int ip = 0;
        // iterate over each octet
        for(String octet : address.split(Pattern.quote("."))) {
            // shift the previously parsed bits over by 1 byte
            ip = ip << 8;
            // set the low order bits to the current octet
            ip |= Integer.parseInt(octet);
        }
        return ip;
    }

    public static void main(String[] args) {
        Firewall fw = new Firewall("test.csv");
        System.out.println(fw.accept_packet("inbound", "tcp", 80, "192.168.1.2"));
        System.out.println(fw.accept_packet("inbound", "udp", 53, "192.168.2.1"));
        System.out.println(fw.accept_packet("outbound", "tcp", 10234, "192.168.10.11"));
        System.out.println(fw.accept_packet("inbound", "tcp", 81, "192.168.1.2"));
        System.out.println(fw.accept_packet("inbound", "udp", 24, "52.12.48.92"));

    }
}
